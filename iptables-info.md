IPTables Info
=============

* Using IPTables is one of the best firewalls for CentOS.  Please following the steps below as a starting point.

#### Install IPTables
* Make sure you have epel-release installed.  If not, make sure you install it before moving forward and update after installing.

```
yum install -y epel-release ; yum update -y
```

* Run the below command to install iptables.

```
yum install -y iptables-services
```

#### Disable FirewallD
* Run the below command to disable FirewallD.

```
systemctl disable firewalld ; systemctl stop firewalld ; systemctl mask firewalld
```

#### Enable and start IPTables
* After disabling FirewallD, make sure you enable and start IPTables.

```
systemctl enable iptables ; systemctl start iptables
```

#### Adding IPTable rules
* After enabling and starting iptables.  Make sure you add some rules in to allow ssh from your network.

* Following the steps to add rules in memory.

```
iptables -I INPUT -s your_external_ip_goes_here -p tcp -m tcp --dport 22 -j ACCEPT
```

* Following the steps to add rules permanently.

```
iptables-save > /etc/sysconfig/iptables
```
